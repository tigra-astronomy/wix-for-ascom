﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2017 TiGra Astronomy, all rights reserved.
// 
// File: AscomDeviceDescriptor.cs  Created: 2017-09-15@18:31
// Last modified: 2017-09-16@01:57 by Tim

using System;
using System.Diagnostics.Contracts;

namespace TA.Ascom.WixCustomActions
    {
    /// <summary>
    ///     Class AscomDeviceDescriptor - represents an ASCOM device driver.
    /// </summary>
    public sealed class AscomDeviceDescriptor : IEquatable<AscomDeviceDescriptor>
        {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AscomDeviceDescriptor" /> class.
        /// </summary>
        /// <param name="deviceId">The device id.</param>
        /// <param name="chooserName">Name of the chooser.</param>
        public AscomDeviceDescriptor(string deviceId, string chooserName)
            {
            Contract.Requires(!string.IsNullOrEmpty(deviceId));
            Contract.Requires(!string.IsNullOrEmpty(chooserName));
            Contract.Requires(!deviceId.Contains(".."));
            DeviceId = deviceId;
            ChooserName = chooserName;
            SetDeviceType();
            }

        /// <summary>
        ///     The ASCOM device ID a.k.a COM ProgID
        /// </summary>
        public string DeviceId { get; }

        /// <summary>
        ///     The human readable name as it will appear in the ASCOM chooser
        /// </summary>
        public string ChooserName { get; }

        /// <summary>
        ///     The type of device that the driver controls; by convention this is the last part of the device ID
        /// </summary>
        public string DeviceType { get; private set; }

        /// <summary>
        ///     Performs very basic validity checking and sets <see cref="DeviceType" /> to the last part of the DeviceId.
        ///     Id DeviceId is "ASCOM.MyName.Rotator" then DeviceType will be "Rotator".
        /// </summary>
        /// <exception cref="System.ArgumentException">
        ///     The DeviceId must contain a minimum of two segments separated by a period. A
        ///     valid DeviceID would be: 'ASCOM.Name.Type'
        /// </exception>
        private void SetDeviceType()
            {
            var delimiters = new[] {'.'};
            var nameParts = DeviceId.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            var countOfParts = nameParts.Length;
            if (countOfParts < 2)
                {
                throw new ArgumentException(
                    "The DeviceId must contain a minimum of two segments separated by a period. A valid DeviceID would be: 'ASCOM.Name.Type'");
                }
            var deviceType = nameParts[countOfParts - 1];
            DeviceType = deviceType;
            }

        /// <summary>
        ///     Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="string" /> that represents this instance.</returns>
        public override string ToString() => string.Format("{0} ({1})", DeviceId, ChooserName);

        #region Equality members
        /// <inheritdoc />
        public bool Equals(AscomDeviceDescriptor other)
            {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(DeviceId, other.DeviceId);
            }

        /// <inheritdoc />
        public override bool Equals(object obj)
            {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((AscomDeviceDescriptor) obj);
            }

        /// <inheritdoc />
        public override int GetHashCode() => DeviceId != null ? DeviceId.GetHashCode() : 0;

        /// <summary>
        ///     Compares two items for equality
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(AscomDeviceDescriptor left, AscomDeviceDescriptor right) => Equals(left, right);

        /// <summary>
        ///     Tests two items for inequality
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(AscomDeviceDescriptor left, AscomDeviceDescriptor right) => !Equals(left, right);
        #endregion Equality members
        }
    }
﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2014 TiGra Astronomy, all rights reserved.
// 
// File: AssemblyInfo.cs  Created: 2014-09-25@00:31
// Last modified: 2014-10-20@20:37 by Fern

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("TA.MeadeUnifiedDriver.Wix.CustomActions")]
[assembly: AssemblyDescription("WIX Custom Actions")]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("12C2516C-FC1C-4F41-8B92-AFB55C3BFE45")]
//[assembly: RegistryPermission(SecurityAction.RequestMinimum, ViewAndModify = @"HKEY_CLASSES_ROOT")]

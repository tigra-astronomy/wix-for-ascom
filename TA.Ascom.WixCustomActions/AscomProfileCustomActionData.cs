﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2014 TiGra Astronomy, all rights reserved.
// 
// File: AscomProfileCustomActionData.cs  Created: 2014-09-25@00:31
// Last modified: 2014-10-20@20:37 by Fern

namespace TA.Ascom.WixCustomActions
    {
    /// <summary>
    ///   Class AscomProfileCustomActionData. Stores all of the data needed to process ASCOM device registration,
    ///   deregistration and profile migration in a deferred custom action.
    /// </summary>
    internal class AscomProfileCustomActionData
        {
        public string DeviceId { get; set; }
        public string ChooserName { get; set; }
        public bool MigrateProfile { get; set; }
        public string ProfileXml { get; set; }
        }
    }

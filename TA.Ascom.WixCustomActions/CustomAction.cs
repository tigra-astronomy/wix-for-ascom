﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2017 TiGra Astronomy, all rights reserved.
// 
// File: CustomAction.cs  Created: 2017-09-15@18:31
// Last modified: 2017-09-16@02:00 by Tim

using System;
using System.Collections.Generic;
using ASCOM;
using ASCOM.Utilities;
using ASCOM.Utilities.Interfaces;
using Microsoft.Deployment.WindowsInstaller;
using Newtonsoft.Json;
using NLog;

namespace TA.Ascom.WixCustomActions
    {
    /// <summary>
    ///     Provides Wix custom installer actions and supporting logic for performing
    ///     driver registration with COM Interop and ASCOM Profile.
    /// </summary>
    public class CustomActions
        {
        private const string AscomDeviceProfiles = "AscomDeviceProfiles";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        ///     Registers the ascom driver with the ASCOM Profile store.
        /// </summary>
        /// <param name="session">The MSI installer session.</param>
        /// <param name="profile"></param>
        /// <returns>Returns an <see cref="ActionResult" /> indicating whether the action was successful.</returns>
        [CustomAction]
        public static ActionResult RegisterAscomDeviceProfiles(Session session, IProfile profile = null)
            {
#if DEBUG_MSI
            MessageBox.Show("RegisterAscomDeviceProfiles: Attach debugger now");
#endif
            var ascomProfile = profile ?? new Profile();
            try
                {
                Log.Info("Commencing registration of ASCOM device profiles (deferred mode custom action)");
                var customActionData = session.CustomActionData[AscomDeviceProfiles];
                var deviceProfiles =
                    JsonConvert.DeserializeObject<List<AscomProfileCustomActionData>>(customActionData);
                foreach (var device in deviceProfiles)
                    {
                    var deviceDescriptor = new AscomDeviceDescriptor(device.DeviceId, device.ChooserName);
                    Log.Info("Attempting to register device profile for {0}", deviceDescriptor);
                    RegisterAscomDeviceProfile(deviceDescriptor, ascomProfile);
                    Log.Info("Registration succeeded for {0}", deviceDescriptor);
                    Log.Info("MigrateProfile is {0}", device.MigrateProfile);
                    if (device.MigrateProfile)
                        ImportDeviceProfile(deviceDescriptor, device.ProfileXml, ascomProfile);
                    Log.Info("Completed processing for {0}", deviceDescriptor);
                    }
                Log.Info("Successfully registered ASCOM device profiles");
                return ActionResult.Success;
                }
            catch (KeyNotFoundException ex)
                {
                Log.Error("The custom action data was empty");
                throw new InstallerException(
                    "No custom action data found when attempting to register ASCOM device profiles. Did you remember to call the EnumerateAscomDeviceProfiles custom action in immediate mode?",
                    ex);
                }
            catch (Exception ex)
                {
                Log.Error(ex, "Failed to register device profiles");
                return ActionResult.Failure;
                }
            }

        private static void ImportDeviceProfile(AscomDeviceDescriptor device, string profileXml, IProfile profile)
            {
            Log.Info("Attempting profile data import for {0}", device);
            profile.DeviceType = device.DeviceType;
            if (!profile.IsRegistered(device.DeviceId))
                {
                Log.Warn("Device profile for {0} not found; this was unexpected. Continuing.", device);
                return;
                }
            Log.Info("Importing profile data: {0}", profileXml);
            profile.SetProfileXML(device.DeviceId, profileXml);
            Log.Info("Profile import succeeded for {0}", device);
            }

        /// <summary>
        ///     Unregisters the ascom driver from COM Interop and the ASCOM Profile store.
        /// </summary>
        /// <param name="session">The MSI installer session.</param>
        /// <param name="ascomProfile">Optional.The ASCOM profile store instance.</param>
        /// <returns>Returns an <see cref="ActionResult" /> indicating whether the action was successful.</returns>
        [CustomAction]
        public static ActionResult UnregisterAscomDeviceProfiles(Session session, IProfile ascomProfile = null)
            {
#if DEBUG_MSI
            MessageBox.Show("UnregisterAscomDeviceProfiles: Attach debugger now");
#endif
            var profileStore = ascomProfile ?? new Profile();

            try
                {
                Log.Info("Commencing deregistration of ASCOM device profiles (deferred mode custom action)");
                var customActionData = session.CustomActionData[AscomDeviceProfiles];
                var deviceProfiles =
                    JsonConvert.DeserializeObject<List<AscomProfileCustomActionData>>(customActionData);
                foreach (var device in deviceProfiles)
                    {
                    var deviceDescriptor = new AscomDeviceDescriptor(device.DeviceId, device.ChooserName);
                    Log.Info("Attempting to deregister device profile for {0}", deviceDescriptor);
                    UnregisterAscomDeviceProfile(deviceDescriptor, profileStore);
                    Log.Info("Deregistration succeeded for {0}", deviceDescriptor);
                    Log.Info("Completed processing for {0}", deviceDescriptor);
                    }
                Log.Info("Successfully Deregistered ASCOM device profiles");
                return ActionResult.Success;
                }
            catch (KeyNotFoundException ex)
                {
                // ReSharper disable once FormatStringProblem
                Log.Warn("No custom action data found in session properties - continuing", ex);
                return ActionResult.Success;
                }
            catch (Exception ex)
                {
                Log.Error(ex, "Failed to deregister device profiles");
                return ActionResult.Failure;
                }
            }

        /// <summary>
        ///     Registers the driver with the ASCOM Profile store. Uses reflection to extract the driver
        ///     DeviceID from the <see cref="DeviceIdAttribute" /> and the driver name from the
        ///     <see cref="ServedClassNameAttribute" />.
        /// </summary>
        /// <param name="driver">The driver descriptor.</param>
        /// <param name="profile"></param>
        /// <exception cref="System.InvalidOperationException">Thrown if the driver type does not bear the required attributes.</exception>
        private static void RegisterAscomDeviceProfile(AscomDeviceDescriptor driver, IProfile profile)
            {
            try
                {
                Log.Info("Attempting to register ASCOM profile for {0}", driver);
                profile.DeviceType = driver.DeviceType;
                if (profile.IsRegistered(driver.DeviceId))
                    Log.Info("ASCOM Profile registration skipped (already registered)");
                else
                    {
                    profile.Register(driver.DeviceId, driver.ChooserName);
                    Log.Info("ASCOM Profile registration succeeded");
                    }
                }
            catch (Exception ex)
                {
                Log.Error(ex, "ASCOM Profile registration failed");
                throw;
                }
            }

        /// <summary>
        ///     Unregisters the driver from the ASCOM profile store.
        /// </summary>
        /// <param name="driver">The driver descriptor.</param>
        /// <param name="profileStore"></param>
        private static void UnregisterAscomDeviceProfile(AscomDeviceDescriptor driver, IProfile profileStore)
            {
            try
                {
                Log.Info("Attempting to unregister ASCOM profile for {0}", driver);
                profileStore.DeviceType = driver.DeviceType;
                if (profileStore.IsRegistered(driver.DeviceId))
                    {
                    profileStore.Unregister(driver.DeviceId);
                    Log.Info("ASCOM Profile deregistration successful.");
                    }
                }
            catch (Exception ex)
                {
                Log.Error(ex, "ASCOM Profile de registration failed");
                throw;
                }
            }

        /// <summary>
        ///     Enumerates the device profiles in the AscomDeviceProfiles table and
        ///     creates custom action data to be processed later in deferred mode.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="ascomProfile">An instance of the ASCOM Profile Store (optional; intended for unit testing).</param>
        /// <returns>ActionResult.</returns>
        [CustomAction]
        public static ActionResult EnumerateAscomDeviceProfiles(Session session, IProfile ascomProfile = null)
            {
            var deviceProfilesToRegister = new List<AscomProfileCustomActionData>();
            var deviceProfilesToUnregister = new List<AscomProfileCustomActionData>();
            var profileStore = ascomProfile ?? new Profile();
#if DEBUG_MSI
            MessageBox.Show("EnumerateAscomDeviceProfiles: Attach debugger now");
#endif
            View drivers;
            try
                {
                drivers = session.Database.OpenView("select * from AscomDeviceProfiles");
                drivers.Execute();
                }
            catch (Exception ex)
                {
                throw new InstallerException(
                    "Please ensure that you  have provided a custom table called AscomDeviceProfiles in your set up project",
                    ex);
                }
            foreach (var driver in drivers)
                {
                using (driver)
                    {
                    var deviceId = driver.GetString(2); // DeviceId
                    var chooserName = driver.GetString(3); // ChooserName
                    var deviceDescriptor = new AscomDeviceDescriptor(deviceId, chooserName);
                    var profileXml = ExportProfileData(deviceDescriptor, profileStore);
                    var profileCustomActionData = new AscomProfileCustomActionData
                        {
                        DeviceId = deviceId,
                        ChooserName = chooserName,
                        MigrateProfile = !string.IsNullOrEmpty(profileXml),
                        ProfileXml = string.IsNullOrEmpty(profileXml) ? string.Empty : profileXml
                        };
                    deviceProfilesToRegister.Add(profileCustomActionData);
                    if (!string.IsNullOrEmpty(profileCustomActionData.ProfileXml))
                        deviceProfilesToUnregister.Add(profileCustomActionData);
                    }
                }
            var registerCustomActionData = new CustomActionData();
            var unregisterCustomActionData = new CustomActionData();

            var json = JsonConvert.SerializeObject(deviceProfilesToRegister);
            registerCustomActionData.Add(AscomDeviceProfiles, json);
            var customActionString = registerCustomActionData.ToString();
            session["caRegisterDrivers"] = customActionString;
            if (deviceProfilesToUnregister.Count > 0)
                {
                json = JsonConvert.SerializeObject(deviceProfilesToUnregister);
                unregisterCustomActionData.Add(AscomDeviceProfiles, json);
                session["caUnregisterDrivers"] = unregisterCustomActionData.ToString();
                }
            return ActionResult.Success;
            }

        /// <summary>
        ///     Exports the profile data for a given device, if it exists.
        /// </summary>
        /// <param name="device">The device descriptor.</param>
        /// <param name="profileStore"></param>
        /// <returns>System.String containing the profile XML if it exists, or null.</returns>
        private static string ExportProfileData(AscomDeviceDescriptor device, IProfile profileStore)
            {
            Log.Info("Attempting profile export for {0}", device);
            try
                {
                profileStore.DeviceType = device.DeviceType;
                if (!profileStore.IsRegistered(device.DeviceId))
                    return null;
                var profileXml = profileStore.GetProfileXML(device.DeviceId);
                Log.Info("Successfully exported profile data: {0}", profileXml);
                return profileXml;
                }
            catch (Exception)
                {
                Log.Warn("Profile export failed for {0} - continuing", device);
                }
            return null;
            }
        }
    }
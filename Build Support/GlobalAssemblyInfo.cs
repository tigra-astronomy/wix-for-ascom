﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2014 TiGra Astronomy, all rights reserved.
// 
// File: GlobalAssemblyInfo.cs  Created: 2014-09-27@00:31
// Last modified: 2014-10-20@20:37 by Fern

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Tigra Astronomy")]
[assembly: AssemblyProduct("WiX Custom Actions for ASCOM")]
[assembly: AssemblyCopyright("Copyright © 2014 Tigra Astronomy, All rights reserved")]
[assembly: AssemblyTrademark("Tigra Astronomy, TiGra Networks")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this type not visible 
// to COM components.  If you need to access a type in this type from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following attributes are overwritten on the build server with values supplied by TeamCity based on the build number.
// 9999.0.0.0 is chosen so that it is obviously a personal build and will override any installed versions on a
// developer workstation.

[assembly: AssemblyVersion("99.99.*")]
[assembly: AssemblyFileVersion("99.99.0.0")]
[assembly: AssemblyInformationalVersion("99.99 uncontrolled build")]
#if DEBUG

[assembly: AssemblyConfiguration("Debug - Uncontrolled build")]
#else

[assembly: AssemblyConfiguration("Release - Uncontrolled build")]
#endif

// Declare 'friend' assemblies that are allowed to see items with internal visibility (for unit testing)

[assembly: InternalsVisibleTo("TA.MeadeUnifiedDriver.Ascom.TelescopeSpecifications")]
[assembly: InternalsVisibleTo("TA.MeadeUnifiedDriver.Server.Specifications")]

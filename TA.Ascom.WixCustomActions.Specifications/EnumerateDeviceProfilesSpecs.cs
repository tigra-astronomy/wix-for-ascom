﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2017 TiGra Astronomy, all rights reserved.
// 
// File: EnumerateDeviceProfilesSpecs.cs  Created: 2017-09-15@18:31
// Last modified: 2017-09-16@01:53 by Tim

using System;
using ASCOM.Utilities.Interfaces;
using FakeItEasy;
using Machine.Specifications;
using Microsoft.Deployment.WindowsInstaller;

namespace TA.Ascom.WixCustomActions.Specifications
    {
    #region context base classes
    public class with_installer_session_and_fake_profile
        {
        protected const string TelescopeId = "ASCOM.UnitTest.Telescope";
        protected const string FocuserId = "ASCOM.UnitTest.Focuser";
        protected const string FakeProfileData = "Fake Profile Data";
        protected const string RegisterActionId = "caRegisterDrivers";
        protected const string UnregisterActionId = "caUnregisterDrivers";
        protected static IProfile FakeProfile;
        protected static Session InstallerSession;
        protected static ActionResult CaResult;

        private Cleanup after = () => InstallerSession.Dispose();

        private Establish context = () => { FakeProfile = A.Fake<IProfile>(); };
        }
    #endregion context base classes

    [Subject(typeof(CustomActions))]
    public class when_enumerating_profiles_to_register_and_the_device_exists : with_installer_session_and_fake_profile
        {
        private Establish context = () =>
            {
            InstallerSession = MsiHelpers.GetSessionFromTestMsi("TA.Ascom.WixCustomActions.SpecimenDriverInstall.msi");
            A.CallTo(() => FakeProfile.IsRegistered(A<string>.Ignored)).Returns(true);
            A.CallTo(() => FakeProfile.GetProfileXML(A<string>.Ignored)).Returns(FakeProfileData);
            };

        private Because of = () => CaResult = CustomActions.EnumerateAscomDeviceProfiles(InstallerSession, FakeProfile);
        private It should_succeed = () => CaResult.ShouldEqual(ActionResult.Success);

        private It should_populate_data_for_the_uninstall_deferred_action =
            () => InstallerSession[UnregisterActionId].ShouldNotBeEmpty();

        private It should_export_the_device_profile =
            () =>
                {
                A.CallTo(() => FakeProfile.GetProfileXML(TelescopeId)).MustHaveHappened(Repeated.Exactly.Once);
                A.CallTo(() => FakeProfile.GetProfileXML(FocuserId)).MustHaveHappened(Repeated.Exactly.Once);
                };

        private It should_put_the_exported_profile_into_the_custom_action_data =
            () => InstallerSession[RegisterActionId].ShouldContain(FakeProfileData);

        private It should_populate_data_for_the_install_deferred_action =
            () => InstallerSession[RegisterActionId].ShouldNotBeEmpty();
        }

    [Subject(typeof(CustomActions), "Immediate mode")]
    public class when_enumerating_profiles_to_register_and_the_device_does_not_exist :
        with_installer_session_and_fake_profile
        {
        private Establish context = () =>
            {
            InstallerSession = MsiHelpers.GetSessionFromTestMsi("TA.Ascom.WixCustomActions.SpecimenDriverInstall.msi");
            A.CallTo(() => FakeProfile.IsRegistered(A<string>.Ignored)).Returns(false);
            };

        private Because of = () => CaResult = CustomActions.EnumerateAscomDeviceProfiles(InstallerSession, FakeProfile);
        private It should_succeed = () => CaResult.ShouldEqual(ActionResult.Success);

        private It should_not_populate_data_for_the_uninstall_deferred_action =
            () => InstallerSession[UnregisterActionId].ShouldBeEmpty();

        private It should_not_export_the_device_profile =
            () => A.CallTo(() => FakeProfile.GetProfileXML(A<string>.Ignored)).MustNotHaveHappened();

        private It should_not_put_the_exported_profile_into_the_custom_action_data =
            () => InstallerSession[RegisterActionId].ShouldNotContain(FakeProfileData);

        private It should_populate_data_for_the_install_deferred_action =
            () => InstallerSession[RegisterActionId].ShouldNotBeEmpty();
        }

    [Subject(typeof(CustomActions), "Missing Table")]
    public class when_the_custom_table_is_missing_from_the_installer : with_installer_session_and_fake_profile
        {
        private Establish context =
            () =>
                {
                InstallerSession =
                    MsiHelpers.GetSessionFromTestMsi("TA.Ascom.WixCustomActions.SpecimenMissingTable.msi");
                };

        private Because of = () =>
            Thrown =
                Catch.Exception(
                    () => { CaResult = CustomActions.EnumerateAscomDeviceProfiles(InstallerSession, FakeProfile); });

        private It should_throw_installer_exception = () => Thrown.ShouldBeOfExactType<InstallerException>();
        private It should_mention_the_table_name = () => Thrown.Message.ShouldContain("AscomDeviceProfiles");
        private static Exception Thrown;
        }
    }
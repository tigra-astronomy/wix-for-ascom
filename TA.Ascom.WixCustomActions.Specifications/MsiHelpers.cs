﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2014 TiGra Astronomy, all rights reserved.
// 
// File: MsiHelpers.cs  Created: 2014-10-14@21:56
// Last modified: 2014-10-20@20:37 by Fern

using System;
using System.IO;
using Machine.Specifications;
using Microsoft.Deployment.WindowsInstaller;
using NLog;

namespace TA.Ascom.WixCustomActions.Specifications
    {
    internal static class MsiHelpers
        {
        internal static Session GetSessionFromTestMsi(string testMsiName)
            {
            var log = LogManager.GetCurrentClassLogger();
            uint product;
            var msiFile = Path.Combine(Environment.CurrentDirectory, testMsiName);
            log.Info("Creating installer session from {0}", msiFile);
            var result = (ActionResult)MsiInterop.MsiOpenPackage(msiFile, out product);
            if (result != ActionResult.Success)
                throw new SpecificationException("Unable to create installer session");
            var session = Session.FromHandle((IntPtr)product, true);
            return session;
            }
        }
    }

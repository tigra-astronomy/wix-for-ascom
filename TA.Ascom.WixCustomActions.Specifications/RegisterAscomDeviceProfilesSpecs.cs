﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2014 TiGra Astronomy, all rights reserved.
// 
// File: RegisterAscomDeviceProfilesSpecs.cs  Created: 2014-10-17@22:43
// Last modified: 2014-10-20@20:37 by Fern

using System;
using FakeItEasy;
using Machine.Specifications;
using Microsoft.Deployment.WindowsInstaller;

namespace TA.Ascom.WixCustomActions.Specifications
    {
    [Subject(typeof(CustomActions), "deferred mode register")]
    public class when_register_is_called_and_there_is_nothing_to_do : with_installer_session_and_fake_profile
        {
        Establish context =
            () =>
                InstallerSession =
                    MsiHelpers.GetSessionFromTestMsi("TA.Ascom.WixCustomActions.SpecimenDriverInstall.msi");

        Because of = () =>
            Thrown =
                Catch.Exception(
                    () => { CaResult = CustomActions.RegisterAscomDeviceProfiles(InstallerSession, FakeProfile); });

        It should_not_register_anything =
            () => A.CallTo(() => FakeProfile.Register(A<string>.Ignored, A<string>.Ignored)).MustNotHaveHappened();

        It should_throw_installer_exception = () => Thrown.ShouldBeOfExactType<InstallerException>();

        It should_mention_the_immediate_mode_custom_action =
            () => Thrown.Message.ShouldContain("EnumerateAscomDeviceProfiles");

        static Exception Thrown;
        }

    [Subject(typeof(CustomActions), "deferred mode register")]
    public class when_register_is_called_and_there_is_valid_custom_action_data : with_installer_session_and_fake_profile
        {
        Establish context = () =>
            {
            InstallerSession = MsiHelpers.GetSessionFromTestMsi("TA.Ascom.WixCustomActions.SpecimenDriverInstall.msi");
            InstallerSession["caRegisterDrivers"] = FakeSessionData;
            InstallerSession.CustomActionData["AscomDeviceProfiles"] = FakeSessionData;
            A.CallTo(() => FakeProfile.IsRegistered(A<string>.Ignored)).ReturnsNextFromSequence(false, true);
            };

        Because of = () => CaResult = CustomActions.RegisterAscomDeviceProfiles(InstallerSession, FakeProfile);

        It should_register_the_telescope =
            () =>
                A.CallTo(() => FakeProfile.Register(TelescopeId, "Fake telescope driver for unit testing"))
                    .MustHaveHappened(Repeated.Exactly.Once);

        It should_succeed = () => CaResult.ShouldEqual(ActionResult.Success);

        It should_import_the_profile_data =
            () =>
                A.CallTo(() => FakeProfile.SetProfileXML(TelescopeId, "fake profile data")).MustHaveHappened(
                    Repeated.Exactly.Once);

        const string FakeSessionData =
            "[{\"DeviceId\":\"ASCOM.UnitTest.Telescope\",\"ChooserName\":\"Fake telescope driver for unit testing\",\"MigrateProfile\":true,\"ProfileXml\":\"fake profile data\"}]";
        }
    }

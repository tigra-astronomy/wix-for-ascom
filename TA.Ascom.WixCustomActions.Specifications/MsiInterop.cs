// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright � 2014 TiGra Astronomy, all rights reserved.
// 
// File: MsiInterop.cs  Created: 2014-10-09@18:34
// Last modified: 2014-10-20@20:41 by Fern

using System.Runtime.InteropServices;

namespace TA.Ascom.WixCustomActions.Specifications
    {
    /// <summary>
    ///   Class exposing static functions and structs from MSI API.
    /// </summary>
    internal static class MsiInterop
        {
        /// <summary>
        ///   PInvoke of MsiOpenPackageW.
        /// </summary>
        /// <param name="packagePath">The path to the package.</param>
        /// <param name="product">A pointer to a variable that receives the product handle.</param>
        /// <returns>Error code.</returns>
        [DllImport("msi.dll", EntryPoint = "MsiOpenPackageW", CharSet = CharSet.Unicode, ExactSpelling = true)]
        internal static extern int MsiOpenPackage(string packagePath, out uint product);
        }
    }

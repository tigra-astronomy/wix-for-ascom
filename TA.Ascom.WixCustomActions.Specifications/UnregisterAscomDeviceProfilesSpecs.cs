﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2014 TiGra Astronomy, all rights reserved.
// 
// File: UnregisterAscomDeviceProfilesSpecs.cs  Created: 2014-10-16@18:26
// Last modified: 2014-10-20@20:37 by Fern

using FakeItEasy;
using Machine.Specifications;
using Microsoft.Deployment.WindowsInstaller;

namespace TA.Ascom.WixCustomActions.Specifications
    {
    [Subject(typeof(CustomActions), "deferred mode unregister")]
    public class when_unregister_is_called_and_there_is_nothing_to_do : with_installer_session_and_fake_profile
        {
        Establish context =
            () =>
                InstallerSession =
                    MsiHelpers.GetSessionFromTestMsi("TA.Ascom.WixCustomActions.SpecimenDriverInstall.msi");

        Because of = () => CaResult = CustomActions.UnregisterAscomDeviceProfiles(InstallerSession, FakeProfile);

        It should_not_unregister_anything =
            () => A.CallTo(() => FakeProfile.Unregister(A<string>.Ignored)).MustNotHaveHappened();

        It should_succeed = () => CaResult.ShouldEqual(ActionResult.Success);
        }

    [Subject(typeof(CustomActions), "deferred mode unregister")]
    public class when_unregister_is_called_and_there_is_valid_custom_action_data :
        with_installer_session_and_fake_profile
        {
        Establish context = () =>
            {
            InstallerSession = MsiHelpers.GetSessionFromTestMsi("TA.Ascom.WixCustomActions.SpecimenDriverInstall.msi");
            InstallerSession["caUnregisterDrivers"] = FakeSessionData;
            InstallerSession.CustomActionData["AscomDeviceProfiles"] = FakeSessionData;
            A.CallTo(() => FakeProfile.IsRegistered(A<string>.Ignored)).Returns(true);
            };

        Because of = () => CaResult = CustomActions.UnregisterAscomDeviceProfiles(InstallerSession, FakeProfile);

        It should_unregister_the_telescope =
            () => A.CallTo(() => FakeProfile.Unregister(A<string>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);

        It should_succeed = () => CaResult.ShouldEqual(ActionResult.Success);

        const string FakeSessionData =
            "[{\"DeviceId\":\"ASCOM.UnitTest.Telescope\",\"ChooserName\":\"Fake telescope driver for unit testing\",\"MigrateProfile\":false,\"ProfileXml\":\"\"}]";
        }
    }

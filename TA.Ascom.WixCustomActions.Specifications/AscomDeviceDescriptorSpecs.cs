﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2014 TiGra Astronomy, all rights reserved.
// 
// File: AscomDeviceDescriptorSpecs.cs  Created: 2014-09-27@01:40
// Last modified: 2014-10-20@20:37 by Fern

using System;
using Machine.Specifications;

namespace TA.Ascom.WixCustomActions.Specifications
    {
    [Subject(typeof(AscomDeviceDescriptor))]
    public class when_descriptor_is_correct : with_devicedescriptor
        {
        Because of = () => Descriptor = new AscomDeviceDescriptor("ASCOM.UnitTest.Telescope", "unit test telescope");

        It should_have_a_type_of_telescope = () => Descriptor.DeviceType.ShouldEqual("Telescope");
        }

    public class with_devicedescriptor
        {
        protected static AscomDeviceDescriptor Descriptor;
        protected static Exception Thrown;
        }

    [Subject(typeof(AscomDeviceDescriptor))]
    public class when_descriptor_is_incorrect : with_devicedescriptor
        {
        Establish context;

        Because of = () => Thrown = Catch.Exception(
            () => Descriptor = new AscomDeviceDescriptor("ASCOM", "unit test telescope"));

        It should_throw_an_argument_exception = () => Thrown.ShouldBeOfExactType<ArgumentException>();
        }

    [Subject(typeof(AscomDeviceDescriptor))]
    public class when_descriptor_ends_in_a_dot : with_devicedescriptor
        {
        Establish context;

        Because of = () => Thrown = Catch.Exception(
            () => Descriptor = new AscomDeviceDescriptor("ASCOM.", "unit test telescope"));

        It should_throw_an_argument_exception = () => Thrown.ShouldBeOfExactType<ArgumentException>();
        }

    [Subject(typeof(AscomDeviceDescriptor))]
    public class when_descriptor_contains_two_consecutive_dots : with_devicedescriptor
        {
        Establish context;

        Because of = () => Thrown = Catch.Exception(
            () => Descriptor = new AscomDeviceDescriptor("ASCOM..focuser", "unit test telescope"));

        It should_throw_an_argument_exception = () => Thrown.ShouldBeOfExactType<ArgumentException>();
        }
    }

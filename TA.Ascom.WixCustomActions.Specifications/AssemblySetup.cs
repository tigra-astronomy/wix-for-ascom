﻿// This file is part of the TA.Ascom.WixCustomActions project
// 
// Copyright © 2017 TiGra Astronomy, all rights reserved.
// 
// File: AssemblySetup.cs  Created: 2017-09-15@18:31
// Last modified: 2017-09-16@01:49 by Tim

using System.Diagnostics;
using Machine.Specifications;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace TA.Ascom.WixCustomActions.Specifications
    {
    public class AssemblySetup : IAssemblyContext
        {
        // ReSharper disable once InconsistentNaming
        private static Logger log;

        public static Logger Log
            {
            get
                {
                if (log == null)
                    ConfigureLogging();
                return log;
                }
            }

        public void OnAssemblyStart()
            {
            Trace.WriteLine("In assembly setup");
            //var fullPath = Path.Combine(Environment.CurrentDirectory, "log4net4mspec.config");
            //var fileInfo = new FileInfo(fullPath);
            ConfigureLogging();
            log.Info("Logging configured");
            }

        public void OnAssemblyComplete()
            {
            log = null;
            }

        private static void ConfigureLogging()
            {
            var config = new LoggingConfiguration();
            var consoleTarget = new ConsoleTarget();
            config.AddTarget("Unit Test", consoleTarget);
            var consoleRule = new LoggingRule("*", LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(consoleRule);
            LogManager.Configuration = config;
            log = LogManager.GetCurrentClassLogger();
            }
        }
    }